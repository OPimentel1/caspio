alert('Here');
var btnEdit = document.querySelector('input[name="Mod0InlineEdit"]');
var btnAdd = document.querySelector('input[name="Mod0InlineAdd"]');
var btnCancel = document.querySelector('input[name="Mod0InlineEditCancel"]');
var v1 = document.querySelectorAll('tr[data-cb-name="data"]');
var v3 = document.querySelector('select[name="InlineAddRoleAtICP"]');
var v4 = document.querySelector('select[name="InlineAddContactRoleAtChurch"]');
var v5 = document.querySelector('input[name="InlineAddWorkPhone"]');
var v6 = document.querySelector('input[name="InlineAddMobile"]');
var v7 = document.querySelector('input[name="InlineAddEmail"]');
var v8 = document.querySelector('input[name="InlineAddFirstName"]');
var v9 = document.querySelector('input[name="InlineAddLastName"]');
var v10 = document.querySelector('input[name="InlineAddAvailableTakeSurvey"]');
var v11 = document.querySelector('select[name="InlineAddStrategyTakeSurvey"]');
var v12 = document.querySelector('select[name="InlineAddTarget_MOther_Daughter_Church"]');
var v23 = document.querySelector('select[name="InlineAddTarget_Place_Attends_Program"]');
var v30 = document.querySelector('input[name="InlineAddCreatedBy"]');
var v31 = document.querySelector('select[name="InlineAddGender"]');
var v32 = document.querySelector('select[name="InlineAddDisability"]');
var v33 = document.querySelector('select[name="InlineAddType_Disability"]');
var v34 = document.querySelector('input[name="InlineAddSpecify_Other_Type_Disability"]');
var v35 = document.querySelector('select[name="InlineAddSpecial_Support_Adapt_Disability"]');
var v36 = document.querySelector('input[name="InlineAddSpecify_Other_Support"]');
var v2 = document.querySelector('input[name="InlineEditAvailableTakeSurvey"]');
var v13 = document.querySelector('select[name="InlineEditTarget_MOther_Daughter_Church"]');
var v14 = document.querySelector('select[name="InlineEditStrategyTakeSurvey"]');
var v15 = document.querySelector('select[name="InlineEditRoleAtICP"]');
var v16 = document.querySelector('select[name="InlineEditContactRoleAtChurch"]');
var v17 = document.querySelector('input[name="InlineEditWorkPhone"]');
var v18 = document.querySelector('input[name="InlineEditMobile"]');
var v19 = document.querySelector('input[name="InlineEditEmail"]');
var v20 = document.querySelector('input[name="InlineEditFirstName"]');
var v21 = document.querySelector('input[name="InlineEditLastName"]');
var v22 = document.querySelector('select[name="InlineEditTarget_Place_Attends_Program"]');
var v24 = document.querySelector('select[name="InlineEditGender"]');
var v25 = document.querySelector('select[name="InlineEditDisability"]');
var v26 = document.querySelector('select[name="InlineEditType_Disability"]');
var v27 = document.querySelector('input[name="InlineEditSpecify_Other_Type_Disability"]');
var v28 = document.querySelector('select[name="InlineEditSpecial_Support_Adapt_Disability"]');
var v29 = document.querySelector('input[name="InlineEditSpecify_Other_Support"]');
var tts = document.querySelector('span[id="tts"]');
var ttsy = document.querySelector('span[id="ttsy"]');
var ttsyp = document.querySelector('span[id="ttsyp"]');
var Wfu = "Waiting for Update";

function setRowColor(x, y) {
	var row = y.parentNode.parentNode;
	switch (x.checked) {
		case true:
			row.style.backgroundColor = "#BEFFD5";
			break;
		default:
			row.style.backgroundColor = "#FFC9BE";
			break;
	}
}

function calcTable(ty, tt) {
	tts.innerHTML = tt;
	ttsy.innerHTML = ty + " ";
	ttsyp.innerHTML = parseInt((ty / tt) * 100) + "%";
}

function validDuplicate(x, y) {
	if (v1) {
		var c = "";
		var fullName = "";
		var z = false;
		var fullNameAdd = x.value + y.value;
		for (let i = 0; i < v1.length; i++) {
			c = v1[i].querySelectorAll('td');
			fullName = c[1].innerHTML + c[2].innerHTML;
			if (fullName.toLowerCase() === fullNameAdd.toLowerCase()) {
				z = true;
			}
		}
		if (z) {
			return false;
		} else {
			return true;
		}
	}
}

if (v2) {
	v2.addEventListener('change', (event) => {
		setRowColor(v2, btnEdit);
		if (v2.checked === false) {
			v13.value = '';
			dsb(v13, "t");
			v14.value = '';
			dsb(v14, "t");
			v22.value = '';
			dsb(v22, "t");
		} else {
			var j = false;
			dsb(v13, "f");
			for (i = 0; i < v13.length; i++) {
				if (v13[i].value === Wfu) {
					j = true;
				}
			}
			if (!j) {
				addWFU(v13);
			}
			v13.value = Wfu;
			j = false;
			dsb(v14, "f");
			for (i = 0; i < v14.length; i++) {
				if (v14[i].value === Wfu) {
					j = true;
				}
			}
			if (!j) {
				addWFU(v14);
			}
			v14.value = Wfu;
			j = false;
			dsb(v22, "f");
			for (i = 0; i < v22.length; i++) {
				if (v22[i].value === Wfu) {
					j = true;
				}
			}
			if (!j) {
				addWFU(v22);
			}
			v22.value = Wfu;
		}
	});
}
if (v13) {
	v13.addEventListener('change', (event) => {
		removeWFU(v13);
	});
}
if (v14) {
	v14.addEventListener('change', (event) => {
		removeWFU(v14);
	});
}
if (v22) {
	v22.addEventListener('change', (event) => {
		removeWFU(v22);
	});
}
if (v24) {
	v24.addEventListener('change', (event) => {
		removeWFU(v24);
	});
}
if (v25) {
	if (v25.value === 'No' || v25.value === "Don't Know") {
		dsb(v26, "t");
		v26.value = '';
		dsb(v27, "t");
		v27.value = '';
	}
	v25.addEventListener('change', (event) => {
		removeBLANK(v25);
		if (v25.value === 'Yes') {
			removeWFU(v26);
			addWFU(v26);
			dsb(v26, "f");
			v26.value = 'Waiting for Update';
		} else {
			dsb(v26, "t");
			v26.value = '';
			dsb(v27, "t");
			v27.value = '';
		}
	});
}
if (v26) {
	if (v26.value != "Other (specify)") {
		dsb(v27, "t");
		v27.value = '';
	}
	v26.addEventListener('change', (event) => {
		removeWFU(v26);
		removeBLANK(v26);
		if (v26.value === 'Other (specify)') {
			dsb(v27, "f");
			v27.value = '';
		} else {
			dsb(v27, "t");
			v27.value = '';
		}
	});
}
if (v28) {
	if (v28.value != "Other (specify)") {
		dsb(v29, "t");
		v29.value = '';
	}
	v28.addEventListener('change', (event) => {
		removeWFU(v28);
		removeBLANK(v28);
		if (v28.value === 'Other (specify)') {
			dsb(v29, "f");
			v29.value = '';
		} else {
			dsb(v29, "t");
			v29.value = '';
		}
	});
}
if (v31) {
	v31.addEventListener('change', (event) => {
		removeWFU(v31);
	});
}
if (v32) {
	if (v32.value === 'No' || v32.value === "Don't Know") {
		dsb(v33, "t");
		v33.value = '';
		dsb(v34, "t");
		v34.value = '';
	}
	v32.addEventListener('change', (event) => {
		removeBLANK(v32);
		if (v32.value === 'Yes') {
			removeWFU(v33);
			addWFU(v33);
			dsb(v33, "f");
			v33.value = 'Waiting for Update';
		} else {
			dsb(v33, "t");
			v33.value = '';
			dsb(v34, "t");
			v34.value = '';
		}
	});
}
if (v33) {
	if (v33.value != "Other (specify)") {
		dsb(v34, "t");
		v34.value = '';
	}
	v33.addEventListener('change', (event) => {
		removeWFU(v33);
		removeBLANK(v33);
		if (v33.value === 'Other (specify)') {
			dsb(v34, "f");
			v34.value = '';
		} else {
			dsb(v34, "t");
			v34.value = '';
		}
	});
}
if (v35) {
	if (v35.value != "Other (specify)") {
		dsb(v36, "t");
		v36.value = '';
	}
	v35.addEventListener('change', (event) => {
		removeWFU(v35);
		removeBLANK(v35);
		if (v35.value === 'Other (specify)') {
			dsb(v36, "f");
			v36.value = '';
		} else {
			dsb(v36, "t");
			v36.value = '';
		}
	});
}
if (btnAdd) {
	orderOPTION(v2, "Other");
	orderOPTION(v9, "Other (specify)");
	orderOPTION(v11, "Other (specify)");
	orderOPTION(v11, "None");
	btnAdd.addEventListener('click', (event) => {
		if (v8.value === "") {
			aMsg("A06");
			event.preventDefault();
		} else if (v9.value === "") {
			aMsg("A07");
			event.preventDefault();
		} else if (!validDuplicate(v8, v9)) {
			aMsg("A01");
			event.preventDefault();
		} else if (v3.value === "") {
			aMsg("A02");
			event.preventDefault();
		} else if (v4.value === "") {
			aMsg("A03");
			event.preventDefault();
		} else if (v5.value === "" && v6.value === "") {
			aMsg("A04");
			event.preventDefault();
		} else if (v7.value === "") {
			aMsg("A05");
			event.preventDefault();
		} else if (v31.value === "" || v31.value === "Waiting for Update") {
			aMsg("A11");
			event.preventDefault();
		} else if (!cMsg("C01")) {
			event.preventDefault();
		} else {
			v30.value = "Church User";
			dsb(v10, "f");
		}
	});
}
if (btnEdit) {
	orderOPTION(v2, "Other");
	orderOPTION(v9, "Other (specify)");
	orderOPTION(v11, "Other (specify)");
	orderOPTION(v11, "None");
	btnEdit.addEventListener('click', (event) => {
		if (v20.value === "") {
			aMsg("A06");
			event.preventDefault();
		} else if (v21.value === "") {
			aMsg("A07");
			event.preventDefault();
		} else if (v15.value === "") {
			aMsg("A02");
			event.preventDefault();
		} else if (v16.value === "") {
			aMsg("A03");
			event.preventDefault();
		} else if (v17.value === "" && v18.value === "") {
			aMsg("A04");
			event.preventDefault();
		} else if (v19.value === "") {
			aMsg("A05");
			event.preventDefault();
		} else if (v13 && v2.checked === true && v13.value === '') {
			aMsg("A08");
			event.preventDefault();
		} else if (v13 && v2.checked === true && v13.value === 'Will not take the Survey') {
			aMsg("A09");
			event.preventDefault();
		} else if (v2.checked === true && v14.value === '') {
			aMsg("A10");
			event.preventDefault();
		} else if (v24.value === "" || v24.value === "Waiting for Update") {
			aMsg("A11");
			event.preventDefault();
		} else if (!cMsg("C03")) {
			event.preventDefault();
		}
	});
}
if (btnCancel) {
	btnCancel.addEventListener('click', (event) => {
		if (!cMsg('C02')) {
			event.preventDefault();
		}
	});
}
document.addEventListener('DataPageReady', (event) => {
	if (v11) {
		v10.checked = true;
		dsb(v10, "t");
		v11.value = "Waiting for Update";
		addWFU(v12);
		addWFU(v13);
		addWFU(v22);
		addWFU(v23);
	}
	if (v1) {
		var cells = "";
		var ty = 0;
		var tt = 0;
		for (let i = 0; i < v1.length; i++) {
			cells = v1[i].querySelectorAll('td');
			switch (cells[10].innerHTML) {
				case xYes:
					v1[i].style.backgroundColor = "#BEFFD5";
					ty++;
					break;
				default:
					v1[i].style.backgroundColor = "#FFC9BE";
					break;
			}
			tt++;
		}
	}
	if (btnEdit) {
		btnEdit.focus();
		setRowColor(v2, btnEdit);
		if (v2.checked === false) {
			dsb(v13, "t");
			v13.value = '';
			dsb(v14, "t");
			v14.value = '';
			dsb(v22, "t");
			v22.value = '';
		} else {
			dsb(v13, "f");
			dsb(v14, "f");
			dsb(v22, "f");
		}
	}
	calcTable(ty, tt);
});
